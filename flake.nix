{
  description = "What Can I Call Myself's nixos configuration";

  outputs =
    {
      self,
      nixpkgs,
      home-manager,
      ...
    }:
    let
      ns = "What Can I Call Myself";
      lib = nixpkgs.lib.extend (final: _: (import ./lib final ns) // home-manager.lib);
      mkHost = lib.${ns}.mkHost self;
    in
    {
      nixosConfigurations = lib.listToAttrs [
        (mkHost "yonderlord" "overlord" "x86_64-linux")
        (mkHost "overlord" "overlord" "x86_64-linux")
      ];
      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixfmt-rfc-style;
    };

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    impermanence.url = "github:nix-community/impermanence";

    sops-nix.url = "github:Mic92/sops-nix";

    stylix.url = "github:danth/stylix";
  };
}
