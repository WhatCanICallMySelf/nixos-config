{
  ns,
  inputs,
  pkgs,
  lib,
  username,
  ...
}:
{
  imports = [
    (lib.mkAliasOptionModule [ ns "home-manager" ] [ "home-manager" "users" username ns ])

    ./hardware-configuration.nix
    ./disko.nix
    ./nvidia.nix
  ];

  boot.supportedFilesystems = [ "ntfs" ];

  ${ns} = {
    core = {
      homeManager.enable = true;
      stateVersion = "24.05";
    };

    home-manager = {
      desktop.hypridle.suspend = false;
      programs = {
        yazi.enable = true;
        git = {
          enable = true;
          username = "What Can I Call Myself";
          email = "WhatCanICallMyself@proton.me";
        };
      };
    };

    system = {
      impermanence.enable = true;
      boot.loader.enable = true;

      pipewire.enable = true;

      opengl.enable = true;

      bluetooth.enable = false;

      desktop = {
        enable = true;
        desktopEnvironment = "hyprland";
      };

      network = {
        enable = true;
        hostId = "53dac3e1";
        firewall.enable = true;
      };

      ssh = {
        agent.enable = true;
        server.enable = true;
      };
    };

    services = {
      cups.enable = false;
      mariadb.enable = true;
      openrgb.enable = true;
    };

    programs = {
      games = {
        gpu-screen-recorder.enable = true;
        heroic.enable = true;
        lunar-client.enable = true;
        osu.enable = true;
        r2modman.enable = true;
        steam.enable = true;
      };
      audacity.enable = true;
      freecad.enable = true;
      geogebra.enable = true;
      inkscape.enable = true;
      kicad.enable = true;
      krita.enable = true;
      libreoffice.enable = true;
      obsidian.enable = true;
      pycharm.enable = true;
      stylix.enable = true;
      virt-manager.enable = true;
      guvcview.enable = true;
      keepassxc.enable = true;
      spotify.enable = true;
      discord.enable = true;
      lazygit.enable = true;
      vial.enable = true;
    };
  };
}
