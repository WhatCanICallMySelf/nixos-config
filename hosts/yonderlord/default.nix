{
  ns,
  inputs,
  lib,
  username,
  ...
}:
{
  imports = [
    (lib.mkAliasOptionModule [ ns "home-manager" ] [ "home-manager" "users" username ns ])
    ./hardware-configuration.nix
  ];

  boot.supportedFilesystems = [ "ntfs" ];
  services.xserver.videoDrivers = [ "amdgpu" ];

  ${ns} = {
    core = {
      homeManager.enable = true;

      stateVersion = "24.05";
    };

    home-manager = {
      programs = {
        yazi.enable = true;
        git = {
          enable = true;
          username = "What Can I Call Myself";
          email = "WhatCanICallMyself@proton.me";
        };
      };
    };

    system = {
      boot.loader.enable = true;

      pipewire.enable = true;

      opengl.enable = true;

      bluetooth.enable = true;

      desktop = {
        enable = true;
        desktopEnvironment = "hyprland";
      };

      network = {
        enable = true;
        hostId = "1df80a3e";
        firewall.enable = true;
      };

      ssh = {
        agent.enable = true;
        server.enable = true;
      };
    };

    services = {
      cups.enable = false;
      mariadb.enable = false;
    };

    programs = {
      games = {
        gpu-screen-recorder.enable = false;
        heroic.enable = true;
        lunar-client.enable = true;
        osu.enable = true;
        r2modman.enable = true;
        steam.enable = true;
      };
      audacity.enable = true;
      freecad.enable = true;
      geogebra.enable = true;
      krita.enable = true;
      inkscape.enable = false;
      kdeconnect.enable = false;
      kicad.enable = false;
      libreoffice.enable = true;
      obsidian.enable = true;
      pycharm.enable = true;
      stylix.enable = true;
      virt-manager.enable = true;
      guvcview.enable = false;
      keepassxc.enable = true;
      spotify.enable = true;
      discord.enable = true;
      lazygit.enable = true;
      vial.enable = true;
    };
  };
}
