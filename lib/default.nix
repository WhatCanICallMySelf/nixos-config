lib: ns: {
  ${ns} = {
    mkHost = self: hostname: username: system: {
      name = hostname;
      value = lib.nixosSystem {
        system = system;
        specialArgs = {
          inherit
            self
            hostname
            username
            lib
            ns
            ;
          inherit (self) inputs;
        };
        modules = [
          ../hosts/${hostname}
          ../modules/nixos
        ];
      };
    };

    # Get list of all nix files and directories in path for easy importing
    scanPaths =
      path:
      map (f: (path + "/${f}")) (
        lib.attrNames (
          lib.filterAttrs (
            path: _type: (_type == "directory") || ((path != "default.nix") && (lib.hasSuffix ".nix" path))
          ) (builtins.readDir path)
        )
      );
  };
}
