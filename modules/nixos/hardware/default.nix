{
  ns,
  lib,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  config = {
    # Replaces the (modulesPath + "/installer/scan/not-detected.nix") import
    hardware.enableRedistributableFirmware = lib.mkDefault true;
  };
}
