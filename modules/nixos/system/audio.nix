{
  ns,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.${ns}.system.pipewire.enable {
    security.rtkit.enable = true;
    services = {
      pulseaudio.enable = false;
      pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
      };
    };

    persistenceHome.directories = [
      ".local/state/wireplumber"
    ];
  };
}
