{
  ns,
  lib,
  config,
  ...
}:
{
  console.keyMap = config.${ns}.system.keyboard.layout;
  # TODO Apperantly needed for keyboard to work
  services.xserver = {
    enable = true;
    xkb = {
      layout = config.${ns}.system.keyboard.layout;
      variant = config.${ns}.system.keyboard.variant;
    };
  };
}
