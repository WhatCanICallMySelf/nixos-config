{
  ns,
  lib,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  options.${ns}.system = {

    impermanence.enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        Whether to enable impermanence. /persist will be used for the persistent filesystem.
      '';
    };

    boot.loader.enable = lib.mkEnableOption "systemd-boot";

    pipewire.enable = lib.mkEnableOption "PipeWire";

    opengl.enable = lib.mkEnableOption "OpenGL";

    bluetooth.enable = lib.mkEnableOption "Bluetooth";

    network = {
      enable = lib.mkEnableOption "NetworkManager" // {
        default = true;
      };

      hostId = lib.mkOption {
        type = lib.types.str;
        description = ''
          Generate with `head -c 8 /etc/machine-id`
        '';
      };
      firewall.enable = lib.mkEnableOption "the firewall" // {
        default = true;
      };
    };

    ssh = {
      server.enable = lib.mkEnableOption "SSH server";
      agent.enable = lib.mkEnableOption "SSH authentication agent";
    };

    time = {
      timeZone = lib.mkOption {
        type = lib.types.str;
        default = "Europe/Oslo";
      };

      defaultLocale = lib.mkOption {
        type = lib.types.str;
        default = "en_US.UTF-8";
      };

      extraLocaleSettings = lib.mkOption {
        type = lib.types.str;
        default = "en_GB.UTF-8";
      };
    };

    keyboard = {
      layout = lib.mkOption {
        type = lib.types.str;
        default = "no";
      };
      variant = lib.mkOption {
        type = lib.types.str;
        default = "nodeadkeys";
      };
    };
  };
}
