{
  ns,
  lib,
  config,
  ...
}:
{
  time.timeZone = config.${ns}.system.time.timeZone;

  i18n.defaultLocale = config.${ns}.system.time.defaultLocale;

  i18n.extraLocaleSettings = {
    LC_ADDRESS = config.${ns}.system.time.extraLocaleSettings;
    LC_IDENTIFICATION = config.${ns}.system.time.extraLocaleSettings;
    LC_MEASUREMENT = config.${ns}.system.time.extraLocaleSettings;
    LC_MONETARY = config.${ns}.system.time.extraLocaleSettings;
    LC_NAME = config.${ns}.system.time.extraLocaleSettings;
    LC_NUMERIC = config.${ns}.system.time.extraLocaleSettings;
    LC_PAPER = config.${ns}.system.time.extraLocaleSettings;
    LC_TELEPHONE = config.${ns}.system.time.extraLocaleSettings;
    LC_TIME = config.${ns}.system.time.extraLocaleSettings;
  };
}
