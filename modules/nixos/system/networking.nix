{
  ns,
  lib,
  config,
  hostname,
  username,
  ...
}:
{
  config = lib.mkMerge [
    (lib.mkIf config.${ns}.system.network.enable {
      users.users.${username}.extraGroups = [ "networkmanager" ];
    })
    {
      networking = {
        networkmanager.enable = config.${ns}.system.network.enable;
        hostName = hostname;
        hostId = config.${ns}.system.network.hostId;
        firewall.enable = config.${ns}.system.network.firewall.enable;
      };
    }
  ];
}
