{
  ns,
  lib,
  config,
  inputs,
  username,
  pkgs,
  ...
}:
let
  # Print all files in the tmpfs file system that will be lost on shutdown
  ephemeralFinder =
    let
      excludePaths = [
        "tmp"
        "root/.cache/nix"
        "home/${username}/.mozilla"
        "home/${username}/.cache/mozilla"
      ];
    in
    pkgs.writeShellApplication {
      name = "ephemeral-finder";
      runtimeInputs = [ pkgs.fd ];
      text = ''
        sudo fd --one-file-system --strip-cwd-prefix --base-directory / --type file \
          --hidden --exclude "{${lib.concatStringsSep "," excludePaths}}" "''${@:1}"
      '';
    };

  # Prints all files and directories in the persistent file system that are not
  # defined as persistent in config
  bloatFinder =
    let
      excludePaths = [
      ];

      persistedFiles = map (
        v: lib.substring 1 (lib.stringLength v.filePath) v.filePath
      ) config.persistence.files;
      persistedDirs = map (
        v: lib.substring 1 (lib.stringLength v.dirPath) v.dirPath
      ) config.persistence.directories;
    in
    pkgs.writeShellApplication {
      name = "bloat-finder";
      runtimeInputs = [ pkgs.fd ];
      text = ''
        sudo fd -au --base-directory /persist --type file --type symlink \
          --exclude "/{${lib.concatStringsSep "," (excludePaths ++ persistedFiles ++ persistedDirs)}}" \
          "''${@:1}"

        # Another pass for empty dirs
        sudo fd -au --base-directory /persist --type empty --type dir \
          --exclude "/{${lib.concatStringsSep "," (excludePaths ++ persistedFiles ++ persistedDirs)}}" \
          "''${@:1}"
      '';
    };
in
{
  imports = [
    inputs.impermanence.nixosModules.impermanence

    (lib.mkAliasOptionModule
      [ "persistence" ]
      [
        "environment"
        "persistence"
        "/persist"
      ]
    )

    (lib.mkAliasOptionModule
      [ "persistenceHome" ]
      [
        "environment"
        "persistence"
        "/persist"
        "users"
        username
      ]
    )
  ];

  config = lib.mkMerge [
    (lib.mkIf config.${ns}.system.impermanence.enable {
      users.users.${username}.packages = [
        ephemeralFinder
        bloatFinder
      ];

      fileSystems."/persist".neededForBoot = true;
      persistence = {
        enable = true;
        hideMounts = true;
        directories = [
          "/var/log"
          "/var/tmp"
          "/var/lib/systemd"
          "/var/lib/nixos"
          "/var/db/sudo/lectured"
        ];
        files = [
          "/etc/machine-id"
          "/etc/adjtime"
        ];

        #users.${username} = lib.mkIf config.${ns}.core.homeManager.enable config.home-manager.users.${username}.${ns}.system.persistence;

        users.${username} = lib.mkMerge [
          {
            directories = [
              # TODO stop having everything in ~/persist and have them their proper places
              "persist"
            ];
          }

          (lib.mkIf config.${ns}.core.homeManager.enable
            config.home-manager.users.${username}.${ns}.system.persistence
          )

        ];
      };
    })

    (lib.mkIf (!config.${ns}.system.impermanence.enable) {
      persistence = {
        enable = false;
        directories = lib.mkForce [ ];
        files = lib.mkForce [ ];
        users.${username} = lib.mkForce [ ];
      };
    })

  ];
}
