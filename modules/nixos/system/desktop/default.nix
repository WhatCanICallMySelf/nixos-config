{
  ns,
  lib,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  options.${ns}.system.desktop = {
    enable = lib.mkEnableOption "Desktop functionality";

    desktopEnvironment = lib.mkOption {
      type = lib.types.enum [
        "hyprland" # Only DE with full config
        "kde"
      ];
      default = null;
    };
  };
}
