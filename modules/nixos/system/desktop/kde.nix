{
  ns,
  lib,
  config,
  pkgs,
  ...
}:
{
  config =
    lib.mkIf
      (config.${ns}.system.desktop.enable && config.${ns}.system.desktop.desktopEnvironment == "kde")
      {
        services.displayManager.sddm.enable = true;
        services.displayManager.sddm.wayland.enable = true;
        services.desktopManager.plasma6.enable = true;
      };
}
