{
  ns,
  lib,
  config,
  pkgs,
  ...
}:
{
  config =
    lib.mkIf
      (config.${ns}.system.desktop.enable && config.${ns}.system.desktop.desktopEnvironment == "hyprland")
      {
        programs.hyprland.enable = true;
        environment.systemPackages = with pkgs; [
          # TODO don't install unused packages
          hypridle
          hyprlock
          hyprpaper
          hyprshot
          hyprpicker
          alacritty
          wofi
          playerctl
          brightnessctl
          pavucontrol
        ];
        security.pam.services.hyprlock = { };
      };
}
