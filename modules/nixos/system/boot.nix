{
  ns,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.${ns}.system.boot.loader.enable {
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
  };
}
