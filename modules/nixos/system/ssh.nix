{
  ns,
  lib,
  pkgs,
  config,
  username,
  ...
}:
{
  # TODO security?
  services.openssh = {
    enable = config.${ns}.system.ssh.server.enable;

    ports = [
      22
    ];

    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      AllowUsers = [
        username
        "root"
      ];
    };
    hostKeys = lib.singleton {
      path = "/etc/ssh/ssh_host_ed25519_key";
      type = "ed25519";
    };
  };

  users.users = {
    ${username}.openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDvpspylh1wus49WyN3Dpgh+FMAfm7xmwqfYKgNwg1qX overlord@overlord"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBf796WvC8O6OOx+C1gEiF178b8LDdR86anGT8UZmx0h overlord@yonderlord"
    ];
    root.openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDvpspylh1wus49WyN3Dpgh+FMAfm7xmwqfYKgNwg1qX overlord@overlord"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBf796WvC8O6OOx+C1gEiF178b8LDdR86anGT8UZmx0h overlord@yonderlord"
    ];
  };

  programs.ssh = {
    startAgent = config.${ns}.system.ssh.agent.enable;
    agentTimeout = "4h";
    pubkeyAcceptedKeyTypes = [ "ssh-ed25519" ];

    knownHosts = {
      "gitlab.com".publicKey =
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf";
    };
  };

  persistence.files = [
    "/etc/ssh/ssh_host_ed25519_key"
    "/etc/ssh/ssh_host_ed25519_key.pub"
  ];

  persistenceHome.directories = [
    {
      directory = ".ssh";
      mode = "0700";
    }
  ];
}
