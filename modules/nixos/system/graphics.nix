{
  ns,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.${ns}.system.opengl.enable {
    hardware.graphics.enable = true;
    hardware.graphics.enable32Bit = true;
  };
}
