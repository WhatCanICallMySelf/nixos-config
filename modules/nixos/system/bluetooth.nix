{
  ns,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.${ns}.system.bluetooth.enable {
    hardware.bluetooth.enable = true;
    hardware.bluetooth.powerOnBoot = true;
  };
}
