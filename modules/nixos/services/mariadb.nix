{
  ns,
  lib,
  config,
  pkgs,
  ...
}:
{
  config = lib.mkIf config.${ns}.services.mariadb.enable {
    services = {
      mysql.enable = true;
      mysql.package = pkgs.mariadb;
    };

    persistence.directories = [ "/var/lib/mysql" ];
  };
}
