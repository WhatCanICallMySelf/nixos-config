{
  lib,
  ...
}:
{
  services.fail2ban = {
    enable = true;
    maxretry = 3;
    bantime-increment.enable = true;

    jails = {
      sshd.settings = {
        mode = "aggressive";
      };
    };
  };
}
