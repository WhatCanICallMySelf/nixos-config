{
  ns,
  lib,
  config,
  pkgs,
  ...
}:
{
  services.hardware.openrgb = {
    enable = config.${ns}.services.openrgb.enable;
    package = pkgs.openrgb-with-all-plugins;
    # TODO make this not hardcoded
    motherboard = "intel";
  };
}
