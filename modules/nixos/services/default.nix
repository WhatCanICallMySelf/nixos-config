{
  ns,
  lib,
  pkgs,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  options.${ns}.services = {
    cups.enable = lib.mkEnableOption "CUPS";
    mariadb.enable = lib.mkEnableOption "MariaDB";
    openrgb.enable = lib.mkEnableOption "OpenRGB";
  };

  config = {
    services = {
      xserver.displayManager.lightdm.enable = false;
      gnome.gnome-keyring.enable = true;

      # Trim them SSDs TODO make option
      fstrim.enable = true;
    };
  };
}
