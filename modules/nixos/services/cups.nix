{
  ns,
  lib,
  config,
  ...
}:
{
  services.printing.enable = config.${ns}.services.cups.enable;
}
