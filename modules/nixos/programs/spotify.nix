{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.spotify.enable {
    users.users.${username}.packages = with pkgs; [
      spotify
    ];

    persistenceHome.directories = [
      ".cache/spotify"
      ".config/spotify"
    ];
  };
}
