{
  ns,
  lib,
  config,
  pkgs,
  ...
}:
{
  programs.zsh = {
    enable = true;
    ohMyZsh = {
      enable = true;
      theme = "robbyrussell";
      plugins = [ "git" ];
    };
  };
  environment.systemPackages = with pkgs; [
    neofetch
    btop
    tree
    tmux
    ripgrep
    fzf
    wl-clipboard
    mpv
    feh
    zip
    unzip
    python3
    fd

    expect
  ];
}
