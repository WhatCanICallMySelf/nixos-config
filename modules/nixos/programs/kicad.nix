{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.kicad.enable {
    users.users.${username}.packages = with pkgs; [
      kicad
    ];
  };
}
