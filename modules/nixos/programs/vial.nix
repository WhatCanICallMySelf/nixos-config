{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.vial.enable {
    users.users.${username}.packages = with pkgs; [
      vial
    ];

    services.udev.packages = with pkgs; [
      vial
      via
    ];
  };
}
