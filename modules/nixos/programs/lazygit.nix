{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.lazygit.enable {
    users.users.${username}.packages = with pkgs; [
      lazygit
    ];

    persistenceHome.directories = [
      ".local/state/lazygit/"
    ];
  };
}
