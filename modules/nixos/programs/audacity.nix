{
  pkgs,
  config,
  lib,
  ns,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.audacity.enable {
    users.users.${username}.packages = with pkgs; [
      audacity
    ];

    persistenceHome.directories = [
      ".config/audacity"
    ];
  };
}
