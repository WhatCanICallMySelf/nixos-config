{
  pkgs,
  config,
  lib,
  ns,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.games.osu.enable {
    users.users.${username}.packages = with pkgs; [
      osu-lazer-bin
    ];

    persistenceHome.directories = [
      ".local/share/osu"
    ];
  };
}
