{
  pkgs,
  config,
  lib,
  ns,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.games.r2modman.enable {
    assertions = [
      {
        assertion = config.${ns}.programs.games.steam.enable;
        message = "Steam must be enabled";
      }
    ];
    users.users.${username}.packages = with pkgs; [
      r2modman
    ];

    persistenceHome.directories = [
      ".config/r2modman"
      ".config/r2modmanPlus-local"
    ];
  };
}
