{
  pkgs,
  config,
  lib,
  ns,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.games.heroic.enable {
    users.users.${username}.packages = with pkgs; [
      heroic
    ];

    persistenceHome.directories = [
      ".config/heroic"
      "Games/Heroic"
    ];
  };
}
