{
  pkgs,
  config,
  lib,
  ns,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.games.steam.enable {
    users.users.${username}.packages = with pkgs; [
      steam
    ];

    persistenceHome.directories = [
      ".local/share/Steam"

      ".factorio"
    ];
  };
}
