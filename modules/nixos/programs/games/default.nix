{
  ns,
  lib,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  options.${ns}.programs.games = {
    gpu-screen-recorder.enable = lib.mkEnableOption "GPU Screen Recorder";
    heroic.enable = lib.mkEnableOption "Heroic";
    lunar-client.enable = lib.mkEnableOption "Lunar Client";
    osu.enable = lib.mkEnableOption "osu!";
    r2modman.enable = lib.mkEnableOption "R2ModMan";
    steam.enable = lib.mkEnableOption "Steam";
  };
}
