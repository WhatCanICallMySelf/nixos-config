{
  pkgs,
  config,
  lib,
  ns,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.games.lunar-client.enable {
    users.users.${username}.packages = with pkgs; [
      lunar-client
    ];
  };
}
