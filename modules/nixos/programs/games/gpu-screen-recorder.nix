{
  pkgs,
  config,
  lib,
  ns,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.games.gpu-screen-recorder.enable {
    environment.systemPackages = with pkgs; [
      gpu-screen-recorder # CLI
      gpu-screen-recorder-gtk # GUI
    ];
  };
}
