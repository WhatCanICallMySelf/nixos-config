{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.pycharm.enable {
    users.users.${username}.packages = with pkgs; [
      jetbrains.pycharm-professional
    ];

    persistenceHome.directories = [
      ".java/"
      ".local/share/JetBrains"
      ".cache/JetBrains"
      ".config/JetBrains"
    ];
  };
}
