{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.inkscape.enable {
    users.users.${username}.packages = with pkgs; [
      inkscape
    ];
  };
}
