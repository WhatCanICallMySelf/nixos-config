{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.geogebra.enable {
    users.users.${username}.packages = with pkgs; [
      geogebra6
    ];
  };
}
