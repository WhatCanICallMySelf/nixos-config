{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.freecad.enable {
    users.users.${username}.packages = with pkgs; [
      freecad
    ];

    persistenceHome.directories = [
      ".config/FreeCAD"
      ".cache/FreeCAD"
    ];
  };
}
