{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.guvcview.enable {
    users.users.${username}.packages = with pkgs; [
      guvcview
    ];
  };
}
