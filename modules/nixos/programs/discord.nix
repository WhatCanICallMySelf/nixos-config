{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.discord.enable {
    users.users.${username}.packages = with pkgs; [
      discord
    ];

    persistenceHome.directories = [
      ".config/discord"
    ];
  };
}
