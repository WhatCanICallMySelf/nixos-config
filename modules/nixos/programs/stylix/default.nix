{
  ns,
  lib,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  options.${ns}.programs.stylix = {
    enable = lib.mkEnableOption "Stylix";
  };
}
