{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.obsidian.enable {
    users.users.${username}.packages = with pkgs; [
      obsidian
    ];

    persistenceHome.directories = [ ".config/obsidian" ];
  };
}
