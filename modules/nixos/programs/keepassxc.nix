{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.keepassxc.enable {
    users.users.${username}.packages = with pkgs; [
      keepassxc
    ];

    persistenceHome.directories = [
      ".config/keepassxc"
      ".cache/keepassxc"
    ];
  };
}
