{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.virt-manager.enable {
    virtualisation.libvirtd.enable = true;
    programs.virt-manager.enable = true;
    users.users.${username}.extraGroups = [ "libvirtd" ];

    persistence.directories = [ "/var/lib/libvirt" ];
  };
}
