{
  ns,
  lib,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  options.${ns}.programs = {
    kdeconnect.enable = lib.mkEnableOption "KDE Connect";
    freecad.enable = lib.mkEnableOption "FreeCAD";
    kicad.enable = lib.mkEnableOption "KiCad";
    audacity.enable = lib.mkEnableOption "Audacity";
    obsidian.enable = lib.mkEnableOption "Obsidian";
    virt-manager.enable = lib.mkEnableOption "virt-manager";
    geogebra.enable = lib.mkEnableOption "Geogebra";
    inkscape.enable = lib.mkEnableOption "Inkscape";
    krita.enable = lib.mkEnableOption "Krita";
    libreoffice.enable = lib.mkEnableOption "LibreOffice";
    pycharm.enable = lib.mkEnableOption "PyCharm";
    guvcview.enable = lib.mkEnableOption "Guvcview";
    keepassxc.enable = lib.mkEnableOption "KeePassXC";
    spotify.enable = lib.mkEnableOption "Spotify";
    discord.enable = lib.mkEnableOption "Discord";
    lazygit.enable = lib.mkEnableOption "Lazygit";
    vial.enable = lib.mkEnableOption "Vial";
  };
}
