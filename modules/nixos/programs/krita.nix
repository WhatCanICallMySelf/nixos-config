{
  pkgs,
  lib,
  ns,
  config,
  username,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.krita.enable {
    users.users.${username}.packages = with pkgs; [
      krita
    ];
  };
}
