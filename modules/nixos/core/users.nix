{
  ns,
  lib,
  config,
  username,
  pkgs,
  ...
}:
{
  users = {
    mutableUsers = false;
    defaultUserShell = pkgs.zsh;
    users = {
      ${username} = {
        isNormalUser = true;
        description = username;
        useDefaultShell = true;
        extraGroups = [ "wheel" ];
        hashedPasswordFile = config.sops.secrets."${username}_hashed_password".path;
      };
      root = {
        hashedPasswordFile = config.sops.secrets."${username}_hashed_password".path;
      };
    };
  };

  sops.secrets."${username}_hashed_password".neededForUsers = true;
}
