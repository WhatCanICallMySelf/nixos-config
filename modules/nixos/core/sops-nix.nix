{
  inputs,
  username,
  pkgs,
  lib,
  ns,
  config,
  ...
}:
{
  imports = [
    inputs.sops-nix.nixosModules.sops
  ];

  environment.systemPackages = with pkgs; [
    sops
    ssh-to-age
  ];

  sops = {
    defaultSopsFile = ../../../secrets/secrets.yaml;
    defaultSopsFormat = "yaml";
    # You have to manualy create the keyfile from the ssh key with:
    # "sudo ssh-to-age -private-key -i /etc/ssh/ssh_host_ed25519_key > ~/.config/sops/age/keys.txt"
    age = {
      keyFile = "${
        lib.optionalString config.${ns}.system.impermanence.enable "/persist"
      }/home/${username}/.config/sops/age/keys.txt";

      sshKeyPaths = [
        "${
          lib.optionalString config.${ns}.system.impermanence.enable "/persist"
        }/etc/ssh/ssh_host_ed25519_key"
      ];
    };
  };

  persistenceHome.directories = [
    ".config/sops/age"
  ];
}
