{
  ns,
  lib,
  inputs,
  config,
  username,
  hostname,
  pkgs,
  ...
}@args:
let
  nixos-config = config;
in
{
  imports = [
    inputs.home-manager.nixosModules.home-manager
  ];

  config = lib.mkIf config.${ns}.core.homeManager.enable {
    home-manager = {
      useGlobalPkgs = true;
      useUserPackages = true;
      backupFileExtension = "backup";

      sharedModules = [ ../../home-manager ];

      extraSpecialArgs = {
        inherit inputs nixos-config;
        inherit (args)
          self
          pkgs
          ns
          username
          ;
      };
    };
  };
}
