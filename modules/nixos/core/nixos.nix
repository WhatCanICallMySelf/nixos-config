{
  ns,
  lib,
  config,
  username,
  pkgs,
  ...
}:
{
  system.stateVersion = config.${ns}.core.stateVersion;
  nixpkgs.config.allowUnfree = true;
  nix = {
    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
      ];
      auto-optimise-store = true;
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
      persistent = true;
      randomizedDelaySec = "15min";
    };
  };
  environment.systemPackages = [ pkgs.nixfmt-rfc-style ];
  users.users.${username}.packages = lib.singleton (
    pkgs.writeShellApplication {
      name = "rb";
      runtimeInputs = [ pkgs.nixos-rebuild ];
      text = ''
        #!/usr/bin/env bash
        set -e
        set -o pipefail

        # TODO Get location from systemInfo.persistLocation instead of hardcoded "/persist/"
        pushd ~/persist/.nixos

        nvim .

        # Early return if no changes were detected
        if git diff --quiet '*.nix'; then
            echo "No changes detected, exiting."
            popd
            exit 0
        fi

        # Autoformat nix files #TODO deal with nixfmt warning
        nix fmt
        # Show changes (why does it fail??)
        git diff -U0 '*.nix' || true

        echo "NixOS Rebuilding..."

        line() {
            printf %"$COLUMNS"s |tr " " "-"
        }

        git add -N .

        sudo unbuffer nixos-rebuild switch --show-trace --flake . 2<&1 | tee nixos-rebuild.log || (grep --color error nixos-rebuild.log && exit 1)
        rm -f nixos-rebuild.log

        current=$(nixos-rebuild list-generations | grep current | grep -o '^\S*')

        git commit -am "$current"

        echo "NixOS Rebuilt OK!"

        # Inspiration: https://gist.github.com/0atman/1a5133b842f929ba4c1e195ee67599d5
      '';
    }
  );
}
