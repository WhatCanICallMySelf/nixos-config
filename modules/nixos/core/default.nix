{
  ns,
  lib,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  options.${ns}.core = {
    homeManager.enable = lib.mkEnableOption "Home Manager";

    stateVersion = lib.mkOption {
      type = lib.types.str;
      readOnly = true;
      example = "24.05";
    };
  };
}
