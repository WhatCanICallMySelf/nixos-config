{ lib, ns, ... }:
{
  options.${ns}.system.persistence = {
    directories = lib.mkOption {
      type = with lib.types; listOf (coercedTo str (d: { directory = d; }) attrs);
      default = [ ];
      example = [
        "Downloads"
        "Music"
        "Pictures"
        "Documents"
        "Videos"
      ];
      description = "Directories relative to home to bind mount in persistent storage";
    };

    files = lib.mkOption {
      type = with lib.types; listOf (coercedTo str (f: { file = f; }) attrs);
      default = [ ];
      example = [ ".screenrc" ];
      description = "Files relative to home to bind mount in persistent storage";
    };
  };
}
