{
  lib,
  nixos-config,
  config,
  ns,
  ...
}:
{
  config =
    lib.mkIf
      (
        nixos-config.${ns}.system.desktop.enable
        && nixos-config.${ns}.system.desktop.desktopEnvironment == "hyprland"
        && config.${ns}.desktop.hypridle.enable
      )
      {
        services = {
          hypridle = {
            enable = true;
            settings = {
              general = {
                lock_cmd = "pidof hyprlock || hyprlock";
                before_sleep_cmd = "loginctl lock-session";
                after_sleep_cmd = "hyprctl dispatch dpms on";
              };
              listener =
                [
                  {
                    timeout = 240;
                    on-timeout = "brightnessctl -s set 10";
                    on-resume = "brightnessctl -r";
                  }
                  {
                    timeout = 300;
                    on-timeout = "loginctl lock-session";
                  }
                  {
                    timeout = 330;
                    on-timeout = "hyprctl dispatch dpms off";
                    on-resume = "hyprctl dispatch dpms on";
                  }
                ]
                ++ lib.optionals (config.${ns}.desktop.hypridle.suspend) [
                  {
                    timeout = 900;
                    on-timeout = "systemctl suspend";
                  }
                ];
            };
          };
        };
      };
}
