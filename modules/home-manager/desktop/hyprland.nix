{
  lib,
  nixos-config,
  config,
  ns,
  ...
}:
{
  config =
    lib.mkIf
      (
        nixos-config.${ns}.system.desktop.enable
        && nixos-config.${ns}.system.desktop.desktopEnvironment == "hyprland"
      )
      {
        assertions = [
          {
            assertion = nixos-config.${ns}.programs.stylix.enable;
            message = "Stylix must be enabled";
          }
        ];
        stylix.targets.hyprland.enable = false;
        ${ns}.desktop = {
          hypridle = {
            enable = lib.mkDefault true;
            suspend = lib.mkDefault true;
          };
          hyprlock.enable = lib.mkDefault true;
          waybar.enable = lib.mkDefault true;
        };
        wayland.windowManager.hyprland = {
          enable = true;
          settings = {
            "$mod" = "SUPER";
            exec-once = [
              "hyprpaper"
              "waybar"
              "hyprctl setcursor ${config.stylix.cursor.name} ${toString config.stylix.cursor.size}"
            ];
            bind =
              [
                "$mod, Q, exec, alacritty"
                "$mod, E, exec, alacritty --command yazi"
                "$mod, R, exec, wofi --show drun -ia"
                "$mod, A, exec, hyprshot -m output --clipboard-only"
                "$mod SHIFT, A, exec, hyprshot -m window --clipboard-only"
                "$mod CTRL, A, exec, hyprshot -m region --clipboard-only"
                "$mod, B, exec, hyprpicker -a"

                "$mod, C, killactive,"
                "$mod, F, fullscreen, 1"
                "$mod SHIFT, F, fullscreen"
                "$mod, J, togglesplit,"
                "$mod, P, pseudo,"
                "$mod CTRL, V, togglefloating"
                "$mod, L, exec, hyprlock"
                "$mod, M, exit,"

                "$mod, left, movefocus, l"
                "$mod, right, movefocus, r"
                "$mod, up, movefocus, u"
                "$mod, down, movefocus, d"

                "$mod SHIFT, right, resizeactive, 10 0"
                "$mod SHIFT, left, resizeactive, -10 0"
                "$mod SHIFT, up, resizeactive, 0 -10"
                "$mod SHIFT, down, resizeactive, 0 10"

                "$mod SHIFT, left, movewindow, l"
                "$mod SHIFT, right, movewindow, r"
                "$mod SHIFT, up, movewindow, u"
                "$mod SHIFT, down, movewindow, d"

                "$mod, S, togglespecialworkspace, magic"
                "$mod SHIFT, S, movetoworkspace, special:magic"

                ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_SINK@ toggle"
                ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_SINK@ 5%- -l 1.0"
                ", XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_SINK@ 5%+ -l 1.0"

                ", XF86AudioMicMute, exec, wpctl set-mute @DEFAULT_SOURCE@ toggle"

                ", XF86AudioPlay, exec, playerctl play-pause"
                ", XF86AudioPrev, exec, playerctl previous"
                ", XF86AudioNext, exec, playerctl next"

                ", XF86MonBrightnessUp, exec, brightnessctl set +5%"
                ", XF86MonBrightnessDown, exec, brightnessctl set 5%-"
              ]
              ++ (
                # workspaces
                # binds $mod + [shift +] {1..9} to [move to] workspace {1..9}
                builtins.concatLists (
                  builtins.genList (
                    i:
                    let
                      ws = i + 1;
                    in
                    [
                      "$mod, ${if ws != 10 then toString ws else toString 0}, workspace, ${toString ws}"
                      "$mod SHIFT, ${if ws != 10 then toString ws else toString 0}, movetoworkspace, ${toString ws}"
                    ]
                  ) 10
                )
              );
            bindm = [
              "$mod, mouse:272, movewindow"
              "$mod, mouse:273, resizewindow"
            ];
            input = {
              kb_layout = "no";
              follow_mouse = "1";
              touchpad.natural_scroll = "yes";
            };
            general = {
              gaps_in = "5";
              gaps_out = "10";
              border_size = "2";
              # Conflicts with default
              "col.active_border" =
                lib.mkForce "rgb(${config.lib.stylix.colors.base0D}) rgb(${config.lib.stylix.colors.base0B}) 45deg";
              "col.inactive_border" = lib.mkForce "rgb(${config.lib.stylix.colors.base03})";
              #"col.active_border" = lib.mkForce "rgba(33ccffee) rgba(00ff99ee) 45deg";
              #"col.inactive_border" = lib.mkForce "rgba(595959aa)";
              layout = "dwindle";
            };
            decoration = {
              rounding = "10";

              blur = {
                enabled = true;
                size = "3";
                passes = "1";
              };
              #drop_shadow = "yes";
              #shadow_range = "4"; TODO this got removed
              #shadow_render_power = "3";
              # Conflicts with default
              #"col.shadow" = lib.mkForce "rgba(1a1a1aee)";
              #"col.shadow" = lib.mkForce "rgb(${config.stylix.generated.palette.base00})";
            };
            animations = {
              enabled = "yes";

              bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";
              animation = [
                "windows, 1, 7, myBezier"
                "windowsOut, 1, 7, default, popin 80%"
                "border, 1, 10, default"
                "borderangle, 1, 8, default"
                "fade, 1, 7, default"
                "workspaces, 1, 6, default"
              ];
            };
            dwindle = {
              pseudotile = "yes";
              preserve_split = "yes";
            };
            xwayland.force_zero_scaling = true;
            monitor = [
              "HDMI-A-1, 2560x1440@144, 0x0, 1"
              "HDMI-A-2, 2560x1440@60, auto-left, 1"
              "eDP-1, preferred, auto, 1"
              ",highres,auto,1"
            ];
            misc = {
              disable_hyprland_logo = true;
              background_color = lib.mkForce "rgb(${config.stylix.generated.palette.base00})";
            };
          };
        };
        services = {
          hyprpaper.enable = true;
        };
      };
}
