{
  lib,
  nixos-config,
  config,
  ns,
  ...
}:
{
  config =
    lib.mkIf
      (
        nixos-config.${ns}.system.desktop.enable
        && nixos-config.${ns}.system.desktop.desktopEnvironment == "hyprland"
        && config.${ns}.desktop.hyprlock.enable
      )
      {
        assertions = [
          {
            assertion = nixos-config.${ns}.programs.stylix.enable;
            message = "Stylix must be enabled";
          }
        ];
        programs = {
          hyprlock = {
            enable = true;
            settings = {
              general = {
                disable_loading_bar = true;
                hide_cursor = true;
                no_fade_in = false;
              };
              background = {
                monitor = "";
                path = lib.mkForce "${config.stylix.image}";
                blur_passes = "0";
              };
              input-field = {
                size = "200, 30";
                position = "0, 20";
                #outer_color = "rgba(00000000)";
                placeholder_text = "<i></i>";
                valign = "bottom";
                monitor = "";
                dots_center = true;
                fade_on_empty = true;
              };
            };
          };
        };
      };
}
