{
  lib,
  nixos-config,
  config,
  ns,
  ...
}:
{
  config =
    lib.mkIf
      (
        nixos-config.${ns}.system.desktop.enable
        && nixos-config.${ns}.system.desktop.desktopEnvironment == "hyprland"
        && config.${ns}.desktop.waybar.enable
      )
      {
        assertions = [
          {
            assertion = nixos-config.${ns}.programs.stylix.enable;
            message = "Stylix must be enabled";
          }
        ];
        stylix.targets.waybar.enable = false;
        programs = {
          waybar = {
            enable = true;
            systemd = {
              enable = false;
              target = "graphical-session.target";
            };
            settings = [
              {
                "position" = "top";
                "layer" = "top";

                modules-left = [
                  "hyprland/workspaces"
                  "hyprland/window"
                ];
                modules-center = [
                  "custom/playerctl"
                ];
                modules-right = [
                  "pulseaudio"
                  "backlight"
                  "network"
                  "clock"
                  "battery"
                  "tray"
                ];
                "hyprland/workspaces" = {
                  "format" = "{name}";
                  "on-click" = "activate";
                };

                "hyprland/window" = {
                  "format" = "";
                };

                "custom/playerctl" = {
                  "format" = "{icon} <span>{}</span>";
                  "return-type" = "json";
                  "max-length" = 55;
                  "exec" =
                    "playerctl -a metadata --format '{\"text\": \" {{markup_escape(artist)}} - {{markup_escape(title)}}\", \"tooltip\": \"{{playerName}} : {{markup_escape(title)}}\", \"alt\": \"{{status}}\", \"class\": \"{{status}}\"}' -F";
                  "on-click-middle" = "playerctl previous";
                  "on-click" = "playerctl play-pause";
                  "on-click-right" = "playerctl next";
                  "format-icons" = {
                    "Paused" = "<span foreground='#${config.stylix.generated.palette.base0C}'></span>";
                    "Playing" = "<span foreground='#${config.stylix.generated.palette.base0B}'></span>";
                  };
                };

                "pulseaudio" = {
                  "format" = "{icon} {volume}% {format_source}";
                  "format-muted" = " Mute {format_source}";
                  "format-bluetooth" = " {volume}% {format_source}";
                  "format-bluetooth-muted" = " Mute {format_source}";
                  "format-source" = " {volume}%";
                  "format-source-muted" = " Mute";
                  "format-icons" = {
                    "headphone" = "";
                    "hands-free" = "";
                    "headset" = "";
                    "phone" = "";
                    "portable" = "";
                    "car" = "";
                    "default" = [
                      ""
                      ""
                      ""
                    ];
                  };
                  "on-click" = "pavucontrol";
                };
                "backlight" = {
                  "format" = "{icon} {percent}%";
                  "format-icons" = [
                    ""
                    ""
                    ""
                    ""
                    ""
                    ""
                    ""
                    ""
                    ""
                  ];
                  "scroll-step" = 0;
                  "tooltip" = false;
                };
                "network" = {
                  "interval" = 1;
                  "format-wifi" = " {essid}";
                  "format-ethernet" = "{ipaddr}/{cidr}";
                  "format-linked" = "󰖪 {ifname} (No IP)";
                  "format-disconnected" = "Disconnected";
                  "format-disabled" = "Disabled";
                  "format-alt" = " {bandwidthUpBytes} |  {bandwidthDownBytes}";
                  "tooltip-format" = "{ipaddr}/{cidr}";
                };
                "clock" = {
                  "interval" = 1;
                  "align" = 0;
                  "rotate" = 0;
                  "format" = " {:%H:%M:%S}";
                  "format-alt" = " {:%a %e/%m/%Y}";
                  "tooltip" = false;
                  #"tooltip-format" = "<big>{:%B %Y}</big>\n<tt><small>{calendar}</small></tt>";
                };
                "battery" = {
                  "interval" = 10;
                  "states" = {
                    "warning" = 20;
                    "critical" = 10;
                  };
                  "format" = "{icon} {capacity}%";
                  "format-warning" = "{icon} {capacity}%";
                  "format-critical" = "{icon} {capacity}%";
                  "format-charging" = "<span font-family='Font Awesome 6 Free'></span> {capacity}%";
                  "format-plugged" = " {capacity}%";
                  "format-alt" = "{icon} {time}";
                  "format-full" = " {capacity}%";
                  "format-icons" = [
                    " "
                    " "
                    " "
                    " "
                    " "
                  ];
                  "tooltip" = false;
                };
                "tray" = {
                  "icon-size" = 15;
                  "spacing" = 5;
                };
              }
            ];
            style = ''
                      * {
                        font-family: "JetBrainsMono Nerd Font";
                        font-size: 15px;
                        font-weight: bold;
                        border-radius: 10px;
                        transition-property: background-color;
                        transition-duration: 0.5s;
                      }
                      @keyframes blink_red {
                        from {
                          color: #${config.lib.stylix.colors.base05};
                        }
                        to {
                          color: #${config.lib.stylix.colors.base08};
                        }
                      }
                      .warning {
                        animation-name: blink_red;
                        animation-duration: 2s;
                        animation-timing-function: linear;
                        animation-iteration-count: infinite;
                        animation-direction: alternate;
                      }
                      .critical, .urgent {
                        animation-name: blink_red;
                        animation-duration: 1s;
                        animation-timing-function: linear;
                        animation-iteration-count: infinite;
                        animation-direction: alternate;
                      }

                      window#waybar {
                        background-color: transparent;
                        border: none;
                      }
                      window > box {
                        margin: 10px 10px 0 10px;
                        background-color: #${config.lib.stylix.colors.base00};
                        border: 2px solid #${config.lib.stylix.colors.base00};
                      }

              	      #waybar.fullscreen #workspaces button.active {
                              background-color: #${config.lib.stylix.colors.base07};
              	      }

                      #workspaces {
                        padding: 0px;
                        background-color: #${config.lib.stylix.colors.base02};
                      }
                      #workspaces button {
                        padding: 1px 6px;
                        margin: 0 5px;
                        background-color: #${config.lib.stylix.colors.base03};
                      }
                      #workspaces button:first-child {
                        margin-left: 0;
                      }
                      #workspaces button:last-child {
                        margin-right: 0;
                      }
                      #workspaces button.active {
                        background-color: #${config.lib.stylix.colors.base0E};
                        color: #${config.lib.stylix.colors.base00};
                      }
                      #workspaces button:hover {
                        background-color:#${config.lib.stylix.colors.base01};
                        color: #${config.lib.stylix.colors.base05};
                      }

                      tooltip {
                        background: #${config.lib.stylix.colors.base00};
                        border: 2px solid #${config.lib.stylix.colors.base03}
                      }

                      #pulseaudio, #backlight, #network, #clock, #battery, #tray{
                        margin: 0px 10px;
                      }

                      #pulseaudio {
                        color: #${config.lib.stylix.colors.base09};
                      }

                      #backlight {
                        color: #${config.lib.stylix.colors.base0A};
                      }

                      #network {
                        color: #${config.lib.stylix.colors.base0B};
                      }

                      #clock {
                        color: #${config.lib.stylix.colors.base0C};
                      }

                      #battery {
                        color: #${config.lib.stylix.colors.base0D};
                      }

                      #tray {
                        padding-left: 0px;
                      }
            '';
          };
        };
      };
}
