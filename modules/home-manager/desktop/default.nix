{
  ns,
  lib,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  options.${ns}.desktop = {
    hypridle = {
      enable = lib.mkEnableOption "hypridle";
      suspend = lib.mkEnableOption "suspending of system";
    };
    hyprlock.enable = lib.mkEnableOption "hyprlock";
    waybar.enable = lib.mkEnableOption "Waybar";
  };
}
