{
  ns,
  nixos-config,
  username,
  ...
}:
{
  home = {
    stateVersion = nixos-config.${ns}.core.stateVersion;

    username = username;
    homeDirectory = "/home/" + username;
  };

  programs.home-manager.enable = true;
}
