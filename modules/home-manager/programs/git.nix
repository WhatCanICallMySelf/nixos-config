{
  config,
  ns,
  ...
}:
{
  programs.git = {
    enable = true;
    userName = config.${ns}.programs.git.username;
    userEmail = config.${ns}.programs.git.email;
    extraConfig = {
      pull.rebase = false;
    };
  };
}
