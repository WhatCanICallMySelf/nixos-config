{
  ns,
  lib,
  ...
}:
{
  imports = lib.${ns}.scanPaths ./.;

  options.${ns}.programs = {
    git = {
      enable = lib.mkEnableOption "Git";

      username = lib.mkOption {
        type = lib.types.str;
      };
      email = lib.mkOption {
        type = lib.types.str;
      };
    };
    yazi.enable = lib.mkEnableOption "Yazi";
  };
}
