{
  systemInfo,
  username,
  ns,
  ...
}:
{
  programs = {
    zoxide = {
      enable = true;
      options = [
        "--cmd cd"
      ];
    };
  };

  ${ns}.system.persistence.directories = [
    ".local/share/zoxide"
  ];
}
