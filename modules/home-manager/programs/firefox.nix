{ ns, ... }:
{
  programs = {
    firefox = {
      enable = true;
      profiles = {
        default = {
          id = 0;
          name = "default";
          isDefault = true;
          # THIS SHOULD BE DEFINED IN FLAKE?????? AND WORK PLS HELP
          #extensions = with pkgs.nur.repos.rycee.firefox-addons; [
          #  ublock-origin
          #];
          search = {
            force = true;
            default = "DuckDuckGo";
            order = [ "DuckDuckGo" ];
          };
        };
      };
    };
  };

  ${ns}.system.persistence.directories = [
    ".mozilla/firefox"
  ];
}
