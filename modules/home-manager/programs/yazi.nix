{
  pkgs,
  lib,
  ns,
  config,
  ...
}:
{
  config = lib.mkIf config.${ns}.programs.yazi.enable {
    programs.yazi = {
      enable = true;
    };
  };
}
