{
  username,
  ...
}:
{
  programs.ssh = {
    enable = true;
    matchBlocks = {
      gitlab = {
        hostname = "gitlab.com";
        identityFile = "~/.ssh/" + username;
      };
    };
  };
}
