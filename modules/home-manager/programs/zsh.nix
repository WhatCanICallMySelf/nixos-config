{
  systemInfo,
  username,
  ns,
  ...
}:
{
  programs = {
    zsh = {
      enable = true;
      oh-my-zsh = {
        enable = true;
        theme = "robbyrussell";
        plugins = [
          "git"
        ];
      };
      shellAliases = {
        # NixOS
        rblg = "lazygit -p ~/persist/.nixos";
        update = "nix flake update --flake ~/persist/.nixos && sudo nixos-rebuild switch --flake ~/persist/.nixos";
        rebuild = "sudo nixos-rebuild switch --flake ~/persist/.nixos";

        v = "nvim";
        lg = "lazygit";

        ssh-add = "ssh-add -t 4h ~/.ssh/" + username;
      };
    };
  };

  ${ns}.system.persistence.files = [
    ".zsh_history"
  ];
}
