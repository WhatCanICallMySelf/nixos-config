import sys
import json


def format_nixos_options(lines):
    tree = {}

    for line in lines:
        line = line.strip()
        if not line:
            continue

        option, value = line.split(" = ")
        keys = option.split('.')

        current_level = tree
        for key in keys[:-1]:
            if key not in current_level:
                current_level[key] = {}
            current_level = current_level[key]

        final_key = keys[-1]
        current_level[final_key] = value.rstrip(';')

    return tree


def collapse_single_element_dicts(tree):
    keys_to_check = list(tree.keys())
    for key in keys_to_check:
        value = tree[key]
        if isinstance(value, dict):
            collapse_single_element_dicts(value)

            if len(value) == 1:
                inner_key = next(iter(value))
                tree[f"{key}.{inner_key}"] = value[inner_key]
                tree.pop(key)
    return tree


def print_tree(tree, indent=0):
    for key, value in tree.items():
        print("  " * indent + f"{key} =", end=" ")

        if isinstance(value, dict):
            print("{")
            print_tree(value, indent + 1)
            print("  " * indent + "};")
        else:
            print(f"{value};")


if __name__ == "__main__":
    # To run use: "nixos-option -r --flake . "What Can I Call Myself" | python generate_custom_module_options_with_default_values.py"
    input = [line.replace("\"What Can I Call Myself\"", "${ns}") for line in sys.stdin.read().strip().splitlines()]

    print_tree(collapse_single_element_dicts(format_nixos_options(input)))

# TODO convert this to a shell script? and automatically update readme with module options
