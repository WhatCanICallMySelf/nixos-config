# Nixos config

## Instaling
Boot nixos minimal iso

Get decent keyboard layout
```
sudo loadkeys no-latin1
```

Clone this repo
```
nix-shell -p git
git clone ...
```

Partition disk
```
sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko nixos-config/hosts/overlord/disko.nix
```

Install nixos
```
sudo nixos-install --flake ./nixos-config#overlord --no-root-password
```

Create folder sturcture and copy nixos-config into /mnt/persist/home/overlord/persist/.nixos


reboot

## Setups TODO
### Overlord
### Yonderlord

# Todo
- [ ] Reformat to use NixOS module options
  - [x] Add more options
  - [x] Fix disko
  - [x] Fix impermanence
  - [ ] Verify/ check module options and whatnot
- [x] Add secrets (sops-nix)
  - [ ] Add wireguard
  - [ ] Encrypt drives
  - [x] Make password a secret...
- [ ] Add custom iso
- [ ] Fully ditch KDE (fix kde-connect?)
- [ ] Notifications?
- [x] Show when window is maximized
- [x] Style/config hyprland cursors etc
- [ ] Firefox extensions
- [x] Create rebuild.sh with nix
- [x] Change formatter to nixfmt
- [x] Change hypridle config to hyprland docs example

# References
Configs that inspired me:
- [https://github.com/JManch/nixos](https://github.com/JManch/nixos) (heavily inspired)
- [https://github.com/ryan4yin/nix-config](https://github.com/ryan4yin/nix-config)
